package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		List<Flower> domList = domController.getList();

		// sort (case 1)
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		packXml(outputXmlFile, domList);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE

		List<Flower> saxList = new ArrayList<>();


		saxList = saxController.getResult();

		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		packXml(outputXmlFile, saxList);



		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE

		List<Flower> staxList = staxController.getResult();
		List<Flower> flows = new ArrayList<>();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		packXml(outputXmlFile, staxList);
	}

	public static void packXml(String pathName, List<Flower> flowers) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
		builderFactory.setNamespaceAware(true);
		DocumentBuilder documentBuilder = builderFactory.newDocumentBuilder();
		Document document = documentBuilder.newDocument();
		Element rootElement = document.createElement("flowers");
		rootElement.setAttribute("xmlns", "http://www.nure.ua");
		rootElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
		rootElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
		document.appendChild(rootElement);
		Element element;
		Element innerElement;
		for(Flower f: flowers) {
			Element flower = document.createElement("flower");
			element = document.createElement("name");
			element.setTextContent(f.getName());
			flower.appendChild(element);

			element = document.createElement("soil");
			element.setTextContent(f.getSoil());
			flower.appendChild(element);

			element = document.createElement("origin");
			element.setTextContent(f.getOrigin());
			flower.appendChild(element);

			element = document.createElement("visualParameters");

			innerElement = document.createElement("stemColour");
			innerElement.setTextContent(f.getVisualParametrs().getStemColour());
			element.appendChild(innerElement);

			innerElement = document.createElement("leafColour");
			innerElement.setTextContent(f.getVisualParametrs().getLeafColour());
			element.appendChild(innerElement);

			innerElement = document.createElement("aveLenFlower");
			innerElement.setTextContent("" + f.getVisualParametrs().getAveLenFlower().getAveLenFlowerVal());
			innerElement.setAttribute("measure", f.getVisualParametrs().getAveLenFlower().getMeasure());
			element.appendChild(innerElement);

			flower.appendChild(element);


			element = document.createElement("growingTips");

			innerElement = document.createElement("tempreture");
			innerElement.setTextContent("" + f.getGrowingTips().getTemperature().getTemperatureVal());
			innerElement.setAttribute("measure", f.getGrowingTips().getTemperature().getMeasure());
			element.appendChild(innerElement);

			innerElement = document.createElement("lighting");
			innerElement.setAttribute("lightRequiring",
					f.getGrowingTips().isLightingRequired() ? "yes" : "no");
			element.appendChild(innerElement);

			innerElement = document.createElement("watering");
			innerElement.setTextContent("" + f.getGrowingTips().getWatering().getWateringVal());
			innerElement.setAttribute("measure", f.getGrowingTips().getWatering().getMeasure());
			element.appendChild(innerElement);

			flower.appendChild(element);

			element = document.createElement("multiplying");
			element.setTextContent(f.getMultiplying());
			flower.appendChild(element);

			rootElement.appendChild(flower);
		}
		StreamResult result = new StreamResult(new File(pathName));
		TransformerFactory tf = TransformerFactory.newInstance();
		javax.xml.transform.Transformer t = tf.newTransformer();
		t.setOutputProperty(OutputKeys.INDENT, "yes");
		t.transform(new DOMSource(document), result);
	}

}
