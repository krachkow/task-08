package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	List<Flower> flowers = new ArrayList<Flower>();


	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void readXml() throws ParserConfigurationException, IOException, SAXException {
		Flower flower = null;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File(xmlFileName));
		document.getDocumentElement().normalize();
		NodeList nList = document.getElementsByTagName("flower");
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node node = nList.item(temp);
			if (node.getNodeType() == Node.ELEMENT_NODE) {
				Element element = (Element) node;
				flower = new Flower();
				flower.setName(element.getElementsByTagName("name").item(0).getTextContent());
				flower.setSoil(element.getElementsByTagName("soil").item(0).getTextContent());
				flower.setOrigin(element.getElementsByTagName("origin").item(0).getTextContent());

				Flower.VisualParametrs visualParametrs = flower.getVisualParametrs();
				visualParametrs.setStemColour(element.getElementsByTagName("stemColour").item(0).getTextContent());
				visualParametrs.setLeafColour(element.getElementsByTagName("leafColour").item(0).getTextContent());
				visualParametrs.getAveLenFlower().setAveLenFlowerVal(
						Integer.parseInt(element.getElementsByTagName("aveLenFlower").item(0).getTextContent()
				));
				visualParametrs.getAveLenFlower().setMeasure(
						element.getElementsByTagName("aveLenFlower").item(0).getAttributes().item(0).getTextContent()
				);

				Flower.GrowingTips growingTips = flower.getGrowingTips();

				growingTips.getTemperature().setTemperatureVal(
						Integer.parseInt(element.getElementsByTagName("tempreture").item(0).getTextContent())
				);
				growingTips.getTemperature().setMeasure(
						element.getElementsByTagName("tempreture").item(0).getAttributes().item(0).getTextContent()
				);

				growingTips.setLightingRequired(
						element.getElementsByTagName("lighting").item(0).getAttributes().item(0).getTextContent().equals("yes")
				);

				growingTips.getWatering().setWateringVal(
						Integer.parseInt(element.getElementsByTagName("watering").item(0).getTextContent())
				);
				growingTips.getWatering().setMeasure(
						element.getElementsByTagName("watering").item(0).getAttributes().item(0).getTextContent()
				);

				flower.setMultiplying(
						element.getElementsByTagName("multiplying").item(0).getTextContent()
				);


				flowers.add(flower);
			}
		}
	}

	public List<Flower> getList() {
		try {
			readXml();
		} catch (ParserConfigurationException | IOException | SAXException e) {
			e.printStackTrace();
		}
		return flowers;
	}


}
