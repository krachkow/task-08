package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private StringBuilder currentValue = new StringBuilder();
	List<Flower> result;
	Flower currentFlower;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE


	public List<Flower> getResult() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser saxParser = factory.newSAXParser();
			saxParser.parse(xmlFileName, this);
		} catch (SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
		}
		return result;
	}


	@Override
	public void startDocument() throws SAXException {
		result = new ArrayList<>();
	}

	@Override
	public void startElement(String uri,
							 String localName,
							 String qName,
							 Attributes attributes) throws SAXException {
		currentValue.setLength(0);
		if (qName.equalsIgnoreCase("flower")) {
			currentFlower = new Flower();
		}

		if (qName.equalsIgnoreCase("aveLenFlower")) {
			String measure = attributes.getValue("measure");
			currentFlower.getVisualParametrs().getAveLenFlower().setMeasure(measure);
		}

		if (qName.equalsIgnoreCase("tempreture")) {
			String measure = attributes.getValue("measure");
			currentFlower.getGrowingTips().getTemperature().setMeasure(measure);
		}

		if (qName.equalsIgnoreCase("lighting")) {
			boolean light = attributes.getValue("lightRequiring").equalsIgnoreCase("yes");
			currentFlower.getGrowingTips().setLightingRequired(light);
		}

		if (qName.equalsIgnoreCase("watering")) {
			String measure = attributes.getValue("measure");
			currentFlower.getGrowingTips().getWatering().setMeasure(measure);
		}
	}

	@Override
	public void endElement(String uri,
						   String localName,
						   String qName) throws SAXException {
		if (qName.equalsIgnoreCase("name")) {
			currentFlower.setName(currentValue.toString());
		};
		if (qName.equalsIgnoreCase("soil")) {
			currentFlower.setSoil(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("origin")) {
			currentFlower.setOrigin(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("stemColour")) {
			currentFlower.getVisualParametrs().setStemColour(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("leafColour")) {
			currentFlower.getVisualParametrs().setLeafColour(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("aveLenFlower")) {
			currentFlower.getVisualParametrs().getAveLenFlower().setAveLenFlowerVal(Integer.parseInt(currentValue.toString()));
		}
		if (qName.equalsIgnoreCase("tempreture")) {
			currentFlower.getGrowingTips().getTemperature().setTemperatureVal(Integer.parseInt(currentValue.toString()));
		}
		if(qName.equalsIgnoreCase("watering")) {
			currentFlower.getGrowingTips().getWatering().setWateringVal(Integer.parseInt(currentValue.toString()));
		}
		if(qName.equalsIgnoreCase("multiplying")) {
			currentFlower.setMultiplying(currentValue.toString());
		}
		if (qName.equalsIgnoreCase("flower")) {
			result.add(currentFlower);
		}
	}

	public void characters(char ch[], int start, int length) {
		currentValue.append(ch, start, length);
	}
}