package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Flower currentFlower;
	private List<Flower> result = new ArrayList<>();

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public List<Flower> getResult() {
		try {
			readXml();
		} catch (FileNotFoundException | XMLStreamException e) {
			e.printStackTrace();
		}
		return result;
	}

	private void readXml() throws FileNotFoundException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(
				new FileInputStream(Paths.get(xmlFileName).toFile())
		);
		int eventType = reader.getEventType();

		while (reader.hasNext()) {
			eventType = reader.next();
			if(eventType == XMLEvent.START_ELEMENT) {
				switch (reader.getName().getLocalPart()) {

					case "flower" :
						currentFlower = new Flower();
						break;
					case "name":
						eventType = reader.next();
						currentFlower.setName(reader.getText());
						break;
					case "soil":
						eventType = reader.next();
						currentFlower.setSoil(reader.getText());
						break;
					case "origin":
						eventType = reader.next();
						currentFlower.setOrigin(reader.getText());
						break;
					case "stemColour":
						eventType = reader.next();
						currentFlower.getVisualParametrs().setStemColour(reader.getText());
						break;
					case "leafColour":
						eventType = reader.next();
						currentFlower.getVisualParametrs().setLeafColour(reader.getText());
						break;
					case "aveLenFlower":
						String measure = reader.getAttributeValue(null,"measure");
						eventType = reader.next();
						currentFlower
								.getVisualParametrs()
								.getAveLenFlower().
								setAveLenFlowerVal(Integer.parseInt(reader.getText()));
						currentFlower
								.getVisualParametrs()
								.getAveLenFlower()
								.setMeasure(measure);
						break;

					case "tempreture":
						String measureT = reader.getAttributeValue(null,"measure");
						eventType = reader.next();
						currentFlower
								.getGrowingTips()
								.getTemperature().
								setTemperatureVal(Integer.parseInt(reader.getText()));
						currentFlower
								.getGrowingTips()
								.getTemperature()
								.setMeasure(measureT);
						break;

					case "lighting":
						String req = reader.getAttributeValue(null, "lightRequiring");
						currentFlower.getGrowingTips().setLightingRequired(req.equals("yes"));
						break;

					case "watering":
						String measureW = reader.getAttributeValue(null,"measure");
						eventType = reader.next();
						currentFlower
								.getGrowingTips()
								.getWatering()
								.setWateringVal(Integer.parseInt(reader.getText()));
						currentFlower
								.getGrowingTips()
								.getWatering()
								.setMeasure(measureW);
						break;

					case "multiplying"	:
						eventType = reader.next();
						currentFlower.setMultiplying(reader.getText());
						break;
				}
			}
			if (eventType == XMLEvent.END_ELEMENT) {
				if (reader.getName().getLocalPart().equals("flower")) {
					result.add(currentFlower);
				}
			}
		}
	}



}