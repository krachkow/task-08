package com.epam.rd.java.basic.task8;

public class Flower {

    String Name;
    String soil;
    String origin;
    VisualParametrs visualParametrs = new VisualParametrs();
    GrowingTips growingTips = new GrowingTips();
    String multiplying;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParametrs getVisualParametrs() {
        return visualParametrs;
    }

    public void setVisualParametrs(VisualParametrs visualParametrs) {
        this.visualParametrs = visualParametrs;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }


    public class VisualParametrs {
        String stemColour;
        String leafColour;
        AveLenFlower aveLenFlower = new AveLenFlower();

        public String getStemColour() {
            return stemColour;
        }

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public AveLenFlower getAveLenFlower() {
            return aveLenFlower;
        }

        public void setAveLenFlower(AveLenFlower aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
        }


        public class AveLenFlower{
            String measure;
            int aveLenFlower;

            public String getMeasure() {
                return measure;
            }

            public void setMeasure(String measure) {
                this.measure = measure;
            }

            public int getAveLenFlowerVal() {
                return aveLenFlower;
            }

            public void setAveLenFlowerVal(int aveLenFlower) {
                this.aveLenFlower = aveLenFlower;
            }
        }
    }

    public class GrowingTips {
        Temperature temperature = new Temperature();
        boolean lightingRequired;
        Watering watering = new Watering();

        public Temperature getTemperature() {
            return temperature;
        }

        public void setTemperature(Temperature temperature) {
            this.temperature = temperature;
        }

        public boolean isLightingRequired() {
            return lightingRequired;
        }

        public void setLightingRequired(boolean lightingRequired) {
            this.lightingRequired = lightingRequired;
        }

        public Watering getWatering() {
            return watering;
        }

        public void setWatering(Watering watering) {
            this.watering = watering;
        }

        public class Temperature{
            int temperature;
            String measure;

            public int getTemperatureVal() {
                return temperature;
            }

            public void setTemperatureVal(int temperature) {
                this.temperature = temperature;
            }

            public String getMeasure() {
                return measure;
            }

            public void setMeasure(String measure) {
                this.measure = measure;
            }
        }

        public class Watering {
            int watering;
            String measure;

            public int getWateringVal() {
                return watering;
            }

            public void setWateringVal(int watering) {
                this.watering = watering;
            }

            public String getMeasure() {
                return measure;
            }

            public void setMeasure(String measure) {
                this.measure = measure;
            }
        }

    }
}
